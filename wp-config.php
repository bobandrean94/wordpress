<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'jari_wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'yp+~+:NwTB |{[%cJoQNiVL&0|TIbK$c$0mBTq?9`1,fZ8GRO1SxY2.,mRSTb0xG' );
define( 'SECURE_AUTH_KEY',  'R{3kz~wv&YG7pu%Xi_UQgOy$nOOY8T|?D@# |&}bqoXhbJ~sp8a?Iy=-|fD0.{T@' );
define( 'LOGGED_IN_KEY',    'bJ]u#Gt&DMj1o=)_::Ro-3#`u$xATAt%;LWhtQFSM{kMe+ecK3@W41yZ-0*b4hVn' );
define( 'NONCE_KEY',        'YK 9`sqYM[C~94<1/W,l9SZ&NVun<XrwwX6vRLF-Ci|>~WOg@ICcwh$VtEKjSPsO' );
define( 'AUTH_SALT',        '-]R=*{P[KZz%%w}0d(3{m8>toKzB7v)M;#Uz0X0d{[:>E9;$QxSE_r!%$r 9Z2t$' );
define( 'SECURE_AUTH_SALT', '9}+D#02HLB4^Y1)(%zb0`Lss)&+IB<=V*Dd<c>9$p=+V-Xjl;JIffkGx62BBtYO7' );
define( 'LOGGED_IN_SALT',   '^.GKzQs#TvtMfI-[^sEN%DZJ6Dx?9t:[H.Q>@V7f+bl=W_<GllKjWd4bY#-ySBa#' );
define( 'NONCE_SALT',       'f%IIiy}SvUPRbbOk[gEljdrjM^vl<&U~o8i~/&a02W4i;}2(8R%*}ow2#E(%r|n0' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */

define('WP_MEMORY_LIMIT', '256M');

define( 'WP_DEBUG', true );
define('WP_DEBUG_LOG', true);

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
